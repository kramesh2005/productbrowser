//
//  Images.swift
//  ProductBrowser
//
//  Created by Ramesh K on 11/07/17.
//  Copyright © 2017 VSS. All rights reserved.
//

import UIKit

class Images: NSObject {
    
    var ImageUrl: String = ""
    
    var Url: String = ""
    
    var Alias: String = ""
    
    var RemoveAfterUpload: Bool = true
    
    var WithCredentials: Bool = true
    
    var DisableMultipart: Bool = true
    
    var Method: String = ""
    
    var isReady: Bool = true
    
    var isUploading: Bool = true
    
    var isUploaded: Bool = true
    
    var isSuccess: Bool = true
    
    var isCancel: Bool = true
    
    var isError: Bool = true
    
    var progress: Int = 0
    

}
