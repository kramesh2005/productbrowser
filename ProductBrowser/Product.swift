//
//  Product.swift
//  ProductBrowser
//
//  Created by Ramesh K on 11/07/17.
//  Copyright © 2017 VSS. All rights reserved.
//

import UIKit

class Product: NSObject {
    
    var Id: String = ""
    
    var Title: String = ""
    
    var Price: String = ""
    
    var OriginalPrice: String = ""
    
    var Description: String = ""
    
    var Quantity: String = ""
    
    var Brand: String = ""
    
    var ModelNumber: String = ""
    
    var FunctionType: String = ""
    
    var Material: String = ""
    
    var Color: String = ""
    
    var JointSize: String = ""
    
    var Height: String = ""
    
    var Diameter: String = ""
    
    var Created: String = ""
    
    var Images: [Images]!

}
