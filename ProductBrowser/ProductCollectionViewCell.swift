//
//  ProductCollectionViewCell.swift
//  ProductBrowser
//
//  Created by Ramesh K on 11/07/17.
//  Copyright © 2017 VSS. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var labDescription: UILabel!
    @IBOutlet var labPrice: UILabel!
    @IBOutlet var labPrevPrice: UILabel!
    
}
