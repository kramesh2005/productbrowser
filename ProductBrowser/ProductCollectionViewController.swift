//
//  ProductCollectionViewController.swift
//  ProductBrowser
//
//  Created by Ramesh K on 11/07/17.
//  Copyright © 2017 VSS. All rights reserved.
//

import UIKit
import SDWebImage

private let reuseIdentifier = "reuseidentifier"

class ProductCollectionViewController: UICollectionViewController {
    
    var arrProduct: [Product] = [Product]()
    
    let objApi: APIAccess = APIAccess()
    
    let objGen: General = General()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
//        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
        
        self.loadProducts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func loadProducts()
    {
        objGen.ShowProgress()
        
        objApi.getProducts { (result) in
            
            self.arrProduct = result
            
            self.objGen.hideProgress()
            
            self.collectionView?.reloadData()
            
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return arrProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let picDimension = (self.view.frame.size.width - 40) / 2
        return CGSize(width: picDimension, height: 184)
        
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProductCollectionViewCell
    
        // Configure the cell
        
        let product =  arrProduct[indexPath.row]
        
        
        cell.labPrice.text = "$\(product.Price)"
        cell.labDescription.text = product.Title
        
        let strikeThroughAttributes = [NSStrikethroughStyleAttributeName : 1]
        let strikeThroughString = NSAttributedString(string: "$\(product.OriginalPrice)", attributes: strikeThroughAttributes)
        
        cell.labPrevPrice.attributedText = strikeThroughString
        
        let strImgURL =  NSString(string:product.Images[0].ImageUrl).addingPercentEscapes(using: String.Encoding.utf8.rawValue)
        
        
        cell.imgProduct.sd_setImage(with: URL(string: strImgURL!), placeholderImage: UIImage(named: "No_Image_Available.png")!)
        
    
        return cell
    }

    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let productViewController = storyboard.instantiateViewController(withIdentifier: "ProductView") as! ProductViewController
        
        
        productViewController.productSelected = arrProduct[indexPath.row]
        
        self.navigationController?.pushViewController(productViewController, animated: true)
    }


    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
