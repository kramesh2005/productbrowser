//
//  Parser.swift
//  ProductBrowser
//
//  Created by Ramesh K on 11/07/17.
//  Copyright © 2017 VSS. All rights reserved.
//

import UIKit

class Parser: NSObject {
    
    func parseDatatoProduct(dataArr: NSArray) -> [Product]
    {
        var arrSeason:[Product] = [Product]()
        
        for index in 0..<dataArr.count
        {
            let produt = Product()
            let data = dataArr[index] as! NSDictionary
            
            produt.Id = String(data.value(forKey: "_id") as! String)
            produt.Title = String(data.value(forKey: "title") as! String)
            produt.Price = String(data.value(forKey: "price") as! Int)
            produt.OriginalPrice = String(data.value(forKey: "originalPrice") as! Int)
            produt.Description = String(data.value(forKey: "description") as! String)
            produt.Quantity = String(data.value(forKey: "quantity") as! Int)
            
            produt.Brand = String(data.value(forKey: "brand") as! String)
            produt.ModelNumber = String(data.value(forKey: "modelNumber") as! String)
            
            produt.FunctionType = String(data.value(forKey: "functionType") as! String)
            
            
            produt.Material = String(data.value(forKey: "material") as! String)

            
            produt.Color = String(data.value(forKey: "color") as! String)

            produt.JointSize = String(data.value(forKey: "jointSize") as! Int)

            produt.Height = String(data.value(forKey: "height") as! Int)

            
            produt.Diameter = String(data.value(forKey: "diameter") as! Int)
            
            
            let arrImg = data.value(forKey: "images") as! NSArray
            
            produt.Images = parseDatatoImages(dataArr: arrImg)
            
            arrSeason.append(produt)
        }
        
        
        return arrSeason;
    }

    
    func parseDatatoImages(dataArr: NSArray) -> [Images]
    {
        var arrSeason:[Images] = [Images]()
        
        for index in 0..<dataArr.count
        {
            let images = Images()
            let data = dataArr[index] as! NSDictionary
            
            
            images.ImageUrl = String(data.value(forKey: "imageUrl") as! String)
            
            
            images.Url = String(data.value(forKey: "url") as! String)
            images.Alias = String(data.value(forKey: "alias") as! String)
            images.RemoveAfterUpload = data.value(forKey: "removeAfterUpload") as! Bool
            images.WithCredentials = data.value(forKey: "withCredentials") as! Bool
            images.DisableMultipart = data.value(forKey: "disableMultipart") as! Bool
            
            images.isReady = data.value(forKey: "isReady")  as! Bool
            images.isUploading = data.value(forKey: "isUploading")  as! Bool
            
            images.isUploaded = data.value(forKey: "isUploaded") as! Bool
            
            
            images.isSuccess = data.value(forKey: "isSuccess")  as! Bool
            
            
            images.isCancel = data.value(forKey: "isCancel")  as! Bool
            
            images.isError = data.value(forKey: "isError")  as! Bool
            
            images.progress = data.value(forKey: "progress")  as! Int
            
            
            arrSeason.append(images)
        }
        
        
        return arrSeason;
    }
}
