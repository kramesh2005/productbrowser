//
//  General.swift
//  Apartment
//

import Foundation
import UIKit
import MRProgress

class General: NSObject {
    
    let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespaces ).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = cString.substring(to: cString.index(cString.startIndex, offsetBy: 1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func ShowProgress()
    {
        let progressView = MRProgressOverlayView.showOverlayAdded(to: appDelegate.window, animated: true)
        
        progressView?.mode = .indeterminateSmall
        progressView?.titleLabelText = "Loading ...";
        
        progressView?.titleLabel.textColor =  UIColor.black // self.hexStringToUIColor(hex: "#1A1F28")
        
        progressView?.tintColor = UIColor.black //self.hexStringToUIColor(hex: "#1A1F28")
        
    }
    
    func hideProgress()
    {
        MRProgressOverlayView.dismissAllOverlays(for: appDelegate.window, animated: true)
    }
    
    func showAlert(message : NSString, title: NSString) // , style: AlertStyle)
    {
                if #available(iOS 8.0, *) {
                    let alertController = UIAlertController(title: title as String, message:
                        message as String, preferredStyle: UIAlertControllerStyle.alert)
        
        
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: nil))
        
                    self.appDelegate.window?.rootViewController!.present(alertController, animated: true, completion:nil)
                } else {
                    // Fallback on earlier versions
                    let alert = UIAlertView()
                    alert.title = title as String
                    alert.message = message as String
                    alert.addButton(withTitle: "Ok")
                    alert.delegate = self
        
                    alert.show()
                }
        
        //        AlertStyle.Error
        
        
        

        
        
    }
    

    
    func showAlertWithComplete(message : String, title: String, completion: @escaping (_ result: Bool) -> Void) // , style: AlertStyle)
    {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: title as String, message:
                message as String, preferredStyle: UIAlertControllerStyle.alert)
            
            
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: {
                action in
                
                completion(true)
                
            }))
            
            self.appDelegate.window?.rootViewController!.present(alertController, animated: true, completion:nil)
        } else {
            // Fallback on earlier versions
            let alert = UIAlertView()
            alert.title = title as String
            alert.message = message as String
            alert.addButton(withTitle: "Ok")
            alert.delegate = self
            
            alert.show()
        }
        
        
    }

    func showConfirmationAlert(message : NSString, title: NSString, completion: @escaping (_ result: Bool) -> Void)
    {
                if #available(iOS 8.0, *) {
                    let alertController = UIAlertController(title: title as String, message:
                        message as String, preferredStyle: UIAlertControllerStyle.alert)
        
        
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler: {
                        action in
                        
                        completion(true)
                        
                    }))
                    
                    alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default,handler: {
                        action in
                        
                        completion(false)
                        
                    }))
                    
                     self.appDelegate.window?.rootViewController!.present(alertController, animated: true, completion:nil)
                   
                } else {
                    // Fallback on earlier versions
                    let alert = UIAlertView()
                    alert.title = title as String
                    alert.message = message as String
                    alert.addButton(withTitle: "Ok")
                    alert.delegate = self
        
                    alert.show()
                }
        
        
        
    }
    
    
    func showConfirmationAlert(_ message : NSString, title: NSString, okText: NSString,  cancelText: NSString, completion: @escaping (_ result: Bool) -> Void)
    {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: title as String, message:
                message as String, preferredStyle: UIAlertControllerStyle.alert)
            
            
            alertController.addAction(UIAlertAction(title: okText as String, style: UIAlertActionStyle.default,handler: {
                action in
                
                completion(true)
                
            }))
            
            alertController.addAction(UIAlertAction(title: cancelText as String, style: UIAlertActionStyle.default,handler: {
                action in
                
                completion(false)
                
            }))
            
            self.appDelegate.window?.rootViewController!.present(alertController, animated: true, completion:nil)
            
        } else {
            // Fallback on earlier versions
            let alert = UIAlertView()
            alert.title = title as String
            alert.message = message as String
            alert.addButton(withTitle: "Ok")
            alert.delegate = self
            
            alert.show()
        }
        
        
        
    }
    
    
    


}


