//
//  ProductViewController.swift
//  ProductBrowser
//
//  Created by Ramesh K on 11/07/17.
//  Copyright © 2017 VSS. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {
    
    @IBOutlet var imgProduct: UIImageView!
    @IBOutlet var labDescription: UILabel!
    @IBOutlet var labPrice: UILabel!
    @IBOutlet var labPrevPrice: UILabel!
    
    
    var productSelected: Product = Product()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.title = productSelected.Title
        
       labPrice.text = "$\(productSelected.Price)"
       labDescription.text = productSelected.Title
        
        let strikeThroughAttributes = [NSStrikethroughStyleAttributeName : 1]
        let strikeThroughString = NSAttributedString(string: "$\(productSelected.OriginalPrice)", attributes: strikeThroughAttributes)
        
        labPrevPrice.attributedText = strikeThroughString
        
        let strImgURL =  NSString(string:productSelected.Images[0].ImageUrl).addingPercentEscapes(using: String.Encoding.utf8.rawValue)
        
        
        imgProduct.sd_setImage(with: URL(string: strImgURL!), placeholderImage: UIImage(named: "No_Image_Available.png")!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
