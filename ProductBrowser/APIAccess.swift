//
//  APIAccess.swift
//  ProductBrowser
//
//  Created by Ramesh K on 11/07/17.
//  Copyright © 2017 VSS. All rights reserved.
//

import UIKit
import Alamofire

class APIAccess: NSObject {
    
    let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
    
    let parser: Parser = Parser()
    
    var commands :HttpCommands = HttpCommands()
    
    var gen: General = General()

    
    func getProducts(completion: @escaping (_ result: [Product]) -> Void)
    {
        
        if(!Reachability.isConnectedToNetwork())
        {
            gen.showAlert(message: "This app required internet connection to run", title: "Error")
            
            completion([Product]())
        }
        else
        {
            
            Alamofire.request(commands.strDomain + commands.strBongList, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
                .responseJSON { response in
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    
                    if let JSON = response.result.value {
                        
                        let arr = JSON as! NSArray
                        
                        let productCategory = self.parser.parseDatatoProduct(dataArr: arr)
                        
                        print(arr)
                        
                        completion(productCategory)
                        
                    }
                    else
                    {
                        completion([Product]())
                    }
            }
            
            
            
        }
    }

}
